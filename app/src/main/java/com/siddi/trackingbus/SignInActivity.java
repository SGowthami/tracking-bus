package com.siddi.trackingbus;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

public class SignInActivity extends AppCompatActivity implements View.OnClickListener {

    EditText et_username, et_password;
    Button submit;

    LocalData localData;
    SharedPreferences.Editor editor;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_signin);

        et_username = findViewById(R.id.et_username);
        et_password = findViewById(R.id.et_password);
        submit = findViewById(R.id.submit);

        submit.setOnClickListener(this);

        localData = new LocalData();
        localData.setContext(this);
        localData.initialisePreference();
        editor = localData.getEditor();


        if (localData.getPreference().getBoolean(Constants.IS_LOGGED_IN, false)) {
            Intent intent = new Intent(this, MainActivity.class);
            startActivity(intent);
            finish();
        }

    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.submit:
                if (validateLogin()) {
                    Toast.makeText(this, "Logged in success", Toast.LENGTH_SHORT).show();

                    editor.putBoolean(Constants.IS_LOGGED_IN, true);
                    editor.commit();

                    Intent intent = new Intent(this, MainActivity.class);
                    startActivity(intent);
                    finish();
                }

                break;
        }
    }

    private boolean validateLogin() {
        boolean isValid = false;

        if (et_username != null && (et_username.getText().toString().equalsIgnoreCase("") || et_username.getText().length() <= 0))
            et_username.setError("Please enter username");
        else if (et_password != null && (et_password.getText().toString().equalsIgnoreCase("") || et_password.getText().length() <= 0))
            et_password.setError("Please enter password");
        else
            isValid = true;

        return isValid;
    }
}