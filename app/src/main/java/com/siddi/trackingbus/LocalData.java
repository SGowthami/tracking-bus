package com.siddi.trackingbus;

import android.app.Application;
import android.content.Context;
import android.content.SharedPreferences;

public class LocalData extends Application {

    SharedPreferences preference;
    Context context;
    SharedPreferences.Editor editor;

    public Context getContext() {
        return context;
    }

    public void setContext(Context context) {
        this.context = context;
    }

    public void initialisePreference() {
        preference = context.getSharedPreferences("Mydata", 0);
        editor = preference.edit();
    }

    public SharedPreferences.Editor getEditor() {
        return editor;
    }

    public SharedPreferences getPreference() {
        return preference;
    }

    public void clearPreferences() {
        editor.clear();
        editor.commit();
    }
}
